# Invoice digitizer service

To get the app running:

* install all the dependencies by running 

    `pip install -r requirements.txt`



* start the server 

    `./manage.py migrate` 

    `./manage.py runserver`


## APIs

* Upload invoice

    ```
    curl -X POST http://localhost:8000/api/invoice/upload/
    ```

* Get all invoice

    ```
    curl -X GET http://localhost:8000/api/invoice/
    ```

* Get invoice detail

    ```
    curl -X GET http://localhost:8000/api/invoice/1/
    ```

* Update invoice data
    ```
    curl -X PUT \
      http://localhost:8000/api/invoice/2/ \
      -H 'Content-Type: application/json' \
      -d '{
        "data":{"Old_balance":400}
    }'
    ```
* Update invoice status as "digitized"
    ```
    curl -X PUT \
      http://localhost:8000/api/invoice/5/ \
      -H 'Content-Type: application/json' \
      -d '{
        "status": "digitized"
    }'
    ```

