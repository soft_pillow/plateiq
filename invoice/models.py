from django.db import models
import uuid
from jsonmerge import merge
# Create your models here.

PROCESSING_STATUS = [ ('in-queue', 'IN QUEUE'),
                      ('in-progress', 'IN PROGRESS'),
                      ('digitized', 'DIGITIZED')]

SAMPLE_INVOICE_DATA = {'DueDate':'2013-02-15','Balance':1990.19,'DocNumber':'SAMP001','Status':'Payable','Meta':{'Description':'Sample Expense','Amount':500,'DetailType':'ExpenseDetail','Customer':'ABC123 (Sample Customer)','Ref':'DEF234 (Sample Construction)','Account':'EFG345 (Fuel)','LineStatus':'Billable','PaymentStatus':'Paid'},'TotalAmt':1990.19}


class Invoice(models.Model):

    status = models.CharField(max_length=30, choices=PROCESSING_STATUS, default='in-queue')
    data = models.JSONField(default=SAMPLE_INVOICE_DATA)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return "{}, {}".format(self.id, self.status)