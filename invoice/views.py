from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Invoice
from .serializers import InvoiceSerializer

# Create your views here.
class InvoiceUpload(APIView):

    def post(self,request):
        # simulate uploading the pdf file
        new_invoice = Invoice()
        new_invoice.save()
        return Response(InvoiceSerializer(new_invoice).data)

class InvoiceList(generics.ListAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


class InvoiceDetail(generics.RetrieveUpdateAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer