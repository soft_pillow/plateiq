from django.urls import path, include
from invoice.views import InvoiceList, InvoiceDetail, InvoiceUpload


urlpatterns = [
    path('invoice/', InvoiceList.as_view()),
    path('invoice/upload/', InvoiceUpload.as_view()),
    path('invoice/<int:pk>/', InvoiceDetail.as_view())
]