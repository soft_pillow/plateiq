from rest_framework import serializers
from .models import Invoice
from jsonmerge import  merge


class InvoiceSerializer(serializers.ModelSerializer):

    data = serializers.JSONField(required=False)

    class Meta:
        model = Invoice
        fields = "__all__"

    def update(self, instance, validated_data):
        # Allow only data and status updates

        if "data" in validated_data:
            # use object merge to retain other data in json
            instance.data = merge(instance.data, validated_data["data"])

        if "status" in validated_data:
            instance.status = validated_data["status"]

        instance.save()
        return instance